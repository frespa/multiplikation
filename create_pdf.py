#!/usr/bin/python3

from fpdf import FPDF
from random import randint

from expressions import RowsOfExpressions

SPLIT_TABLE_DATA= (
    (" ",)
)

pdf = FPDF()

pdf.add_page()

pdf.set_font("helvetica", size=12)


with pdf.table(
    RowsOfExpressions(),
    first_row_as_headings=False,
    gutter_height=3,
    gutter_width=3
):
    pass
for _ in range(3):

    with pdf.table(
        SPLIT_TABLE_DATA,
        first_row_as_headings=False,
        borders_layout="INTERNAL"
    ):
        pass

    with pdf.table(
        RowsOfExpressions(),
        first_row_as_headings=False,
        gutter_height=3,
        gutter_width=3
    ):
        pass

pdf.output("result.pdf")