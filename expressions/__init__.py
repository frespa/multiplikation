from random import randint


class Expression:
    """
    This is a simple representation of a multiplication expression
    """
    def __init__(self) -> None:
        self.a = randint(0,10)
        self.b = randint(0,10)

    @property
    def c(self) -> int:
        return self.a * self.b

    @property
    def expression(self) -> str:
        return f"{self.a} x {self.b} = "

    @property
    def solution(self) -> str:
        return str(self.c)

    def __str__(self) -> str:
        return f"{self.a} x {self.b} = "

    def encode(self, encoding):
        return self.__str__().encode(encoding)

    def __eq__(self, __value: object) -> bool:
        return self.a == __value.a and self.b == __value.b


class RowOfExpressions:
    """
    This represents a row of expressions.
    """
    def __init__(self) -> None:
        self.row = (Expression() for _ in range(6))

    def __iter__(self):
        return iter(self.row)


class RowsOfExpressions:
    """
    This represents multiple rows of expressions.
    """
    def __init__(self) -> None:
        self.rows = (RowOfExpressions() for _ in range(5))

    def __iter__(self):
        return iter(self.rows)
